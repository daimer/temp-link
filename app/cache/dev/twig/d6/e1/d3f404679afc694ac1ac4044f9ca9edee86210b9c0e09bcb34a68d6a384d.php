<?php

/* default/registration.html */
class __TwigTemplate_d6e1d3f404679afc694ac1ac4044f9ca9edee86210b9c0e09bcb34a68d6a384d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("base.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div id=\"content\">
    \t<p id=\"connect-text\">
    \t\tNo account matches.
    \t</p>
    \t<p id=\"connect-text\">
    \t\t<a href=\"/confirm-registration\">Create this account</a>
    \t</p>
    </div>
";
    }

    public function getTemplateName()
    {
        return "default/registration.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 4,  36 => 3,  11 => 1,);
    }
}
