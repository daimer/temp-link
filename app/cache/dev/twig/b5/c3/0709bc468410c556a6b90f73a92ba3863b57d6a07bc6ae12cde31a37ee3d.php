<?php

/* header-connected.html */
class __TwigTemplate_b5c30709bc468410c556a6b90f73a92ba3863b57d6a07bc6ae12cde31a37ee3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"header\">
    <h1>Temporary links</h1>
\t\t<div id=\"user\" class=\"user\">
\t    \t<p>";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "login", array()));
        echo " <a href=\"/logout\">log out</a></p>
\t    </div>
 </div>";
    }

    public function getTemplateName()
    {
        return "header-connected.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 4,  19 => 1,);
    }
}
