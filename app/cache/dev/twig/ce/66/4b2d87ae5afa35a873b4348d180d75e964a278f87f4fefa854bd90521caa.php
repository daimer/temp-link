<?php

/* default/list.html */
class __TwigTemplate_ce664b2d87ae5afa35a873b4348d180d75e964a278f87f4fefa854bd90521caa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("base-connected.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base-connected.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "\t<div id=\"create-link\">
\t\t<form action=\"/create-link\" method=\"post\">
\t\t\t<input type=\"text\" placeholder=\"deep link\" name=\"target\"  value=\"\"/>
    \t\t<input type=\"text\" placeholder=\"comment\"   name=\"comment\" value=\"\"/>
    \t\t<input type=\"submit\" value=\"create link\"   name=\"submit\"  class=\"submit\"/>
\t\t</form>
\t</div>
\t
    <div id=\"content\">
    \t";
        // line 13
        if ((isset($context["links"]) ? $context["links"] : $this->getContext($context, "links"))) {
            // line 14
            echo "    \t\t<div id=\"list-links\">
    \t\t\t";
            // line 15
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : $this->getContext($context, "links")));
            foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
                // line 16
                echo "\t\t            <p>deep link: ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["link"], "target", array()));
                echo "</p>
\t\t            <!-- <p>gen link: http://domain.com/r/";
                // line 17
                echo twig_escape_filter($this->env, $this->getAttribute($context["link"], "hash", array()));
                echo "</p>-->
\t\t            <p>gen link: http://localhost:8000/r/";
                // line 18
                echo twig_escape_filter($this->env, $this->getAttribute($context["link"], "hash", array()));
                echo "</p>
\t\t            <p>";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute($context["link"], "comment", array()));
                echo "</p>
\t\t            <a href=\"/delete/";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute($context["link"], "id", array()), "html", null, true);
                echo "\">delete</a>
\t\t            <a href=\"/renew/";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($context["link"], "id", array()), "html", null, true);
                echo "\">renew</a>
\t\t            <hr />
\t\t        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "    \t\t</div>
\t\t";
        } else {
            // line 26
            echo "\t\t<p id=\"connect-text\">
\t\t    You have no links.
\t\t</p>
\t\t";
        }
        // line 30
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "default/list.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 30,  93 => 26,  89 => 24,  80 => 21,  76 => 20,  72 => 19,  68 => 18,  64 => 17,  59 => 16,  55 => 15,  52 => 14,  50 => 13,  39 => 4,  36 => 3,  11 => 1,);
    }
}
