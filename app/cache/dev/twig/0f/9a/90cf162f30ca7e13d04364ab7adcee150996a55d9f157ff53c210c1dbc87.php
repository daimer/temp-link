<?php

/* default/404.html */
class __TwigTemplate_0f9a90cf162f30ca7e13d04364ab7adcee150996a55d9f157ff53c210c1dbc87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("reset.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />
\t\t<link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
    \t<div id=\"content\">
    \t<p id=\"connect-text\">
    \t\tLink not found.
    \t</p>
    </div>
    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "BBK links!";
    }

    public function getTemplateName()
    {
        return "default/404.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 5,  39 => 9,  35 => 8,  31 => 7,  26 => 5,  20 => 1,);
    }
}
