<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\User;
use AppBundle\Entity\Link;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
	
	protected $_pdo;
	
	protected function getDb()
	{
		if (!$this->_pdo) {
			$this->_pdo = new \PDO(
				'mysql:host=127.0.0.1;dbname=bbk',
			    'root', null
			);
		}
		return $this->_pdo;
	}
	
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {	
    	$session = new Session();
		if ($session->has('user')) {
			return $this->redirect($this->generateUrl('list'));
		}
        return $this->render('default/index.html');
    }
	
	/**
     * @Route("/connect", name="connect")
     */
    public function connectAction(Request $request)
    {
    	if (!$request->request->get('login') 
    	|| !$request->request->get('pswd')) {
    		return $this->render('default/index.html');
    	} 
		
    	$session = new Session();
		
		$stmt = $this->getDb()->prepare('select * from user where login = :login');
		$stmt->execute(['login' => $request->request->get('login')]);
		$result = $stmt->fetchAll();
			 
		if ($result && $result[0]['password'] !== $request->request->get('pswd')) {
			return $this->render('default/index.html');
		}
		
		if (!$result) {
			$session->set('registration', [
				'login'    => $request->request->get('login'), 
		    	'password' => $request->request->get('pswd'),
			]);
			return $this->redirect($this->generateUrl('registration'));
		}
		
		$session->set('user', [
			'id'	   => $result[0]['id'],
			'login'    => $result[0]['login'], 
	    	'password' => $result[0]['password'],
		]);
		return $this->redirect($this->generateUrl('list'));
    }
	
	/**
     * @Route("/registration", name="registration")
     */
    public function registrationAction()
    {
    	$session = new Session();
		if (!$session->has('registration')) {
			return $this->redirect($this->generateUrl('homepage'));
		}
        return $this->render('default/registration.html');
    }
	
	/**
     * @Route("/confirm-registration", name="confirm-registration")
     */
    public function confirmRegistrationAction()
    {
    	$session = new Session();
		if (!$session->has('registration')) {
			return $this->redirect($this->generateUrl('homepage'));
		}
		
		$data = $session->get('registration');
		$stmt = $this->getDb()->prepare('insert into user (login, password) VALUES (:login, :password)');
		$stmt->execute(['login' => $data['login'], 'password' => $data['password']]);
		$session->set('user', $data + ['id' => $this->getDb()->lastInsertId()]);
		
		$session->remove('registration');
		return $this->redirect($this->generateUrl('list'));
    }
	
	/**
     * @Route("/list", name="list")
     */
    public function listAction()
    {
    	$session = new Session();
		if (!$session->has('user')) {
			return $this->redirect($this->generateUrl('homepage'));
		}
		
		$stmt = $this->getDb()->prepare('select * from link where user_id = :uid');
		$stmt->execute(['uid' => $session->get('user')["id"]]);
		$links = $stmt->fetchAll();
		
        return $this->render('default/list.html', [
        	'user'  => $session->get('user'),
        	'links' => $links ? $links : [],
        ]);
    }
    
    /**
     * @Route("/create-link", name="create-link")
     */
    public function createLinkAction(Request $request)
    {
    	$session = new Session();
		$user = $session->get('user');
    	if (!$request->request->get('target') || !isset($user['id'])) {
    		return $this->redirect($this->generateUrl('list'));
    	}
		
		$stmt = $this->getDb()->prepare('insert into link (
				target, comment, date_update, hash, user_id
			) VALUES (:target, :comment, :date_update, :hash, :user_id)'
		);
		$stmt->execute([
			'target'  => $request->request->get('target'), 
			'comment' => $request->request->get('comment'),
			'date_update' => time(),
			'hash'        => hash('sha1', uniqid()),
			'user_id'     => $user['id']
		]);
		
		return $this->redirect($this->generateUrl('list'));
    }
	
	/**
     * @Route("/renew/{id}", name="renew")
     */
    public function renewAction($id)
    {
    	$session = new Session();
		
		$stmt = $this->getDb()->prepare('select * from link where id = :lid');
		$stmt->execute(['lid' => $id]);
		$link = $stmt->fetchAll();
		
		if (!$link || $link[0]['user_id'] != $session->get('user')['id']) {
			return $this->redirect($this->generateUrl('list'));
		}
		
		$stmt = $this->getDb()->prepare('UPDATE link set hash=:hash, date_update=:date where id=:lid limit 1');
		$stmt->execute(['hash' => hash('sha1', uniqid()), 'lid' => $id, 'date' => time()]);
		
		return $this->redirect($this->generateUrl('list'));
    }
	
	/**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction($id)
    {
    	$session = new Session();
		
		$stmt = $this->getDb()->prepare('select * from link where id = :lid');
		$stmt->execute(['lid' => $id]);
		$link = $stmt->fetchAll();
		
		if (!$link || $link[0]['user_id'] != $session->get('user')['id']) {
			return $this->redirect($this->generateUrl('list'));
		}
		
		$stmt = $this->getDb()->prepare('DELETE FROM link where id=:lid limit 1');
		$stmt->execute(['lid' => $id]);
		
		return $this->redirect($this->generateUrl('list'));
    }
	
	/**
     * @Route("/r/{hash}", name="redirect")
     */
    public function rebounceAction($hash)
    {
    	$session = new Session();
		
		$stmt = $this->getDb()->prepare('select * from link where hash = :hash');
		$stmt->execute(['hash' => $hash]);
		$link = $stmt->fetchAll();
		
		if (!$link) {
			return $this->render('default/404.html');
		}
		
		if (($link[0]['date_update'] + 60 * 60 * 2) < time()) {
			return $this->render('default/404.html');
		}
		
		return $this->redirect($link[0]['target']);
    }

	/**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
    	$session = new Session();
		$session->remove('user');
		return $this->redirect($this->generateUrl('homepage'));
    }
}
