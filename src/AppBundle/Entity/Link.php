<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Link
 *
 * @ORM\Table(link)
 * @ORM\Entity
 */
class Link
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
    protected $target;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $hash;

    /**
     * @ORM\Column(type="text")
     */
    protected $comment;
	
	/**
     * @ORM\Column(type="integer")
     */
    protected $user_id;
	
	/**
     * @ORM\Column(type="integer")
     */
    protected $date_update;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function getTarget()
    {
        return $this->target;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function getHash()
    {
        return $this->hash;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function getComment()
    {
        return $this->comment;
    }
    
	/**
     * Get id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function setTarget($target)
    {
        return $this->target = $target;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function setHash($hash)
    {
        return $this->hash = $hash;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function setComment($comment)
    {
        return $this->comment = $comment;
    }
    
	/**
     * Get id
     *
     * @return integer 
     */
    public function setUserId($userId)
    {
        return $this->user_id = $userId;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function setDateUpdate($timestamp)
    {
        return $this->date_update = $timestamp;
    }
}
