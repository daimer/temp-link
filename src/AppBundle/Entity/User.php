<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $login;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $password;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function setLogin($login)
    {
        return $this->login = $login;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function setPassword($pswd)
    {
        return $this->password = $pswd;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function getLogin()
    {
        return $this->login;
    }
	
	/**
     * Get id
     *
     * @return integer 
     */
    public function getPassword()
    {
        return $this->password;
    }
}
